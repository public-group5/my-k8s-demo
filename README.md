### Pipeline for K8s demo

This Gitlab-CI pipeline is aiming for a K8s demo which will
 * Create a k8s cluster using kops
 * Deploy a nginx application
 * Deploy monitoring and alerting 
 * Configure monitoring and alerting
 * Encrypt using aws kms and create a k8s secret
  
#### Prepare the IAM user
Before launching the Pipeline, an AWS IAM user needs to be created with all the policies listed in the following example:
```sh
aws iam create-group --group-name kops
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess --group-name kops
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonRoute53FullAccess --group-name kops
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --group-name kops
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/IAMFullAccess --group-name kops
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonVPCFullAccess --group-name kops
aws iam create-user --user-name kops
aws iam add-user-to-group --user-name kops --group-name kops
aws iam create-access-key --user-name kops
```
In the end of the IAM user creation, the AccessKeyId andSecret AccessKey will be generated. Please input that respectively into pipeline variables described in the table of section "Set variables for the Pipeline".

#### Create KMS key and grant IAM user permission
In order to use the aws kms, a key and its policy need to be created:
```sh
aws kms create-key --description "k8s demo"
```
after this an alias needs to be assigned to this key:
```sh
aws kms create-alias --target-key-id KEY_ID --alias-name "alias/k8sdemo"
```
in the end a policy needs to be attached to the key to grant permissions for IAM user kops. An example policy is as follow:
```sh
{
    "Id": "key-consolepolicy",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Allow use of the key",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::497761050936:user/kops"
            },
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt",
                "kms:ReEncrypt*",
                "kms:DescribeKey"
            ],
            "Resource": "*"
        }
    ]
}
```
#### Set variables for the Pipeline
The following variables need to be set for the pipeline:

| Name | Content | Protected |
| ------ | ------ | ------ |
| ACCESSKEYID | aws access key id | yes |
| SECRETACCESSKEY | aws access key | yes |
| SMTPSERVER | The alert sender SMTP server | no |
| SENDERMAILADDRESS | The alert sender email address | no |
| RECIEVERMAILADDRESS | The alert reciever email address | no |
| SENDERMAILPASSWORD | The alert sender email password | yes |
| ALERTHTTPENDPOINT | The alert HTTP endpoint | no |
| DEMOSECRET | The secret to be encrypted and created for application| yes |

Now the pipeline is ready to be launched.
